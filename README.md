# Prueba Ionic

Aplicación de prueba hecha en IONIC 5.

  - La aplicación carga un Array de 4.000 elementos que se muestran por pantalla
  - La aplicación tiene implentada un buscador

# New Features!

  - Al hacer scroll el header (donde está incluido el navbar) se ocultará
  - Se puede hacer clicks en los elementos para ampliar las imágenes

### Tech

* [IONIC] - Ionic Framework!

### Installation

Ionic 5 requires [Node.js](https://nodejs.org/) v4+ to run.

Install the dependencies and start the server.

```sh

$ git clone 
$ cd test_app_san
$ npm install
$ ionic serve -c || ionic serve -l
```

No environments setted...

```sh
Running app in mobile:
$ npm install @capacitor/cli @capacitor/core
$ npx cap init
$ npx cap add ios
$ npx cap add android

Examples:
$ ionic capacitor run
$ ionic capacitor run android
$ ionic capacitor run android -l --external
$ ionic capacitor run ios --livereload --external
$ ionic capacitor run ios --livereload-url=http://localhost:8100
```

### Plugins

test_app is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin | README |
| ------ | ------ |
| lorem-ipsum | https://www.npmjs.com/package/lorem-ipsum |






[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [IONIC]: <https://ionicframework.com/>
   

