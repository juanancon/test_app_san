import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DataNotDisplayedComponent } from './data-not-displayed.component';

describe('DataNotDisplayedComponent', () => {
  let component: DataNotDisplayedComponent;
  let fixture: ComponentFixture<DataNotDisplayedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataNotDisplayedComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DataNotDisplayedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should have an ion-item', () => {
    const el: HTMLElement = fixture.nativeElement;
    const item = el.querySelector('ion-item');
    const spinner = el.querySelector('ion-spinner');
    const label = el.querySelector('ion-label');

    fixture.detectChanges();
    expect(item).not.toBeNull();
    expect(spinner).not.toBeNull();
    expect(label).not.toBeNull();

  });
});
