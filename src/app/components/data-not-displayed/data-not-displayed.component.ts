import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-not-displayed',
  templateUrl: './data-not-displayed.component.html',
  styleUrls: ['./data-not-displayed.component.scss'],
})
export class DataNotDisplayedComponent implements OnInit {

  public noResults = 'No se han encontrado resultados';

  constructor() { }

  ngOnInit() {}

}
