import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImageItemComponent } from './image-item.component';

describe('ImageItemComponent', () => {
  let component: ImageItemComponent;
  let fixture: ComponentFixture<ImageItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageItemComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImageItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should have an ion-item', () => {
    const el: HTMLElement = fixture.nativeElement;
    const item = el.querySelector('ion-item');
    const thumb = el.querySelector('ion-thumbnail');
    const img = el.querySelector('ion-img');
    const text = el.querySelector('ion-text');

    fixture.detectChanges();

    expect(item).not.toBeNull();
    expect(thumb).not.toBeNull();
    expect(img).not.toBeNull();
    expect(text).not.toBeNull();
  });

});
