import { ImageModalPage } from '@app/image-modal/image-modal.page';
import { ModalController } from '@ionic/angular';
import { Image } from '@interfaces/image.interface';
import * as Constant from '@services/const';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-image-item',
  templateUrl: './image-item.component.html',
  styleUrls: ['./image-item.component.scss'],
})
export class ImageItemComponent implements OnInit {
  @Input() item: Image;

  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {}

  onError(item: Image) {
    item.photo = Constant.IMG_DEFAULT;
  }

  async openModal(item: Image) {
    const modal = await this.modalCtrl.create({
      component: ImageModalPage,
      componentProps: {
        img: item
      }
    });
    return await modal.present();
  }

}
