import { Injectable } from '@angular/core';
import { LoremIpsum } from 'lorem-ipsum';

@Injectable({
  providedIn: 'root'
})
export class LoremService {

  private lorem: LoremIpsum;
  private sentence: string;

  constructor() {
    this.initParagraph();
   }

   initParagraph() {
    this.lorem = new LoremIpsum({
      sentencesPerParagraph: {
        max: 6,
        min: 4
      },
      wordsPerSentence: {
        max: 6,
        min: 4
      }
    });
   }

   getLoremSentence(): string {
     this.sentence = this.lorem.generateSentences(1);
     return this.sentence;
   }

}
