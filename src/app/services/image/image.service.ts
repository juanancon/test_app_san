import { Image } from '@interfaces/image.interface';
import * as Constant from '@services/const';

import { Injectable } from '@angular/core';
import { LoremService } from './lorem.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public items: Array<Image> = new Array<Image>();
  private apiUrl = Constant.IMAGE_API_ENDPOINT;
  private size: string = Constant.IMAGE_SIZE;
  private index = 4000;

  constructor(private loremSer: LoremService) {}

  getImages(): Array<Image> {
    for (let i = 0; i < this.index; i++) {
      const sentence = this.loremSer.getLoremSentence();
      this.items.push({
        id: `${i}`,
        photo: `${this.apiUrl}/${i}/${this.size}`,
        text: sentence
      });
    }
    return this.items;
  }

}
