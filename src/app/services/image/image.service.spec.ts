import { TestBed } from '@angular/core/testing';
import { ImageService } from './image.service';

describe('ImageService', () => {
  let service: ImageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Get Images return an Object', () => {
    expect(typeof service.getImages()).toBe('object');
  });

  it('GetImages Should return a 4000 items Array', () => {
    const result = service.getImages();

    // tslint:disable-next-line: no-unused-expression
    expect(Array.isArray(result)).toBeTruthy;
    expect(result.length).toEqual(4000);
  });

});
