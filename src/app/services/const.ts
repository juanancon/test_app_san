// constants.ts
export const IMAGE_API_ENDPOINT = 'https://picsum.photos/id';
export const IMAGE_SIZE = '500/500';
export const IMG_DEFAULT = 'assets/imgs/default.png';
