import { ImageService } from '@app/services/image/image.service';
import { Component, Input, ViewChild } from '@angular/core';
import { Image } from '@interfaces/image.interface';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public items: Array<Image> = [];
  public displayItems: Array<Image> = [];
  public dataDisplayed = true;
  public placeholdertext = 'Inserte ID o texto';

  constructor(private imgService: ImageService,
              private modalController: ModalController) {
    this.initializeItems();
  }

  initializeItems() {
    this.items = this.imgService.getImages();
    this.displayItems = this.items;
  }

  searchInput(ev: any) {
    const searchQuery = ev.target.value.trim().toUpperCase();
    this.displayItems = this.items
    .filter((item) => {
      const searchedText = item.text + item.id;
      return searchedText.toUpperCase().includes(searchQuery);
    });
    this.dataDisplayed = this.displayItems.length === 0 ? false : true;
  }

}
