import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';
import { ImageService } from '@app/services/image/image.service';


describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let imageService;

  const ImageServiceStub = {
    getImages: () => []
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomePage ],
      imports: [IonicModule.forRoot()],
      providers: [
        {provide: ImageService, useValue: ImageServiceStub}
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    imageService = TestBed.inject(ImageService);

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Items and DisplayItems should be an no-empty array', () => {
    expect(component.items.length).toBeDefined();
    expect(component.displayItems.length).toBeDefined();

    expect(Array.isArray(component.items)).toBeTruthy();
    expect(Array.isArray(component.displayItems)).toBeTruthy();
  });

  it('getImages obtain an Image Item', () => {
    spyOn(imageService, 'getImages').and.returnValue([Image]);
  });

  it('Should have an searchbar', () => {
    const el: HTMLElement = fixture.nativeElement;
    const bar = el.querySelector('ion-searchbar');
    fixture.detectChanges();
    expect(bar).not.toBeNull();
  });

});
