import { HideHeaderDirective } from './../directives/hide-header.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { DataNotDisplayedComponent } from './../components/data-not-displayed/data-not-displayed.component';
import { ImageItemComponent } from './../components/image-item/image-item.component';
import { HomePageRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
  ],
  declarations: [
    HomePage,
    ImageItemComponent,
    DataNotDisplayedComponent,
    HideHeaderDirective
  ]
})
export class HomePageModule {}
